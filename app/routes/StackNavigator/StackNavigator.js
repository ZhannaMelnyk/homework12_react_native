import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ContactDetailsView from '../../views/ContactDetailsView/ContactDetailsView';
import ContactListView from '../../views/ContactListView/ContactListView';
import { NavigationContainer } from '@react-navigation/native'

const Stack = createStackNavigator()

function StackNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='ContactList'>
        <Stack.Screen name='ContactList' component={ContactListView} />
        <Stack.Screen name='ContactDetails' component={ContactDetailsView} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default StackNavigator;