import React, { Component } from 'react';
import StackNavigator from '../../routes/StackNavigator/StackNavigator'

class Root extends Component {
  render() {
    return (
      <StackNavigator />
    );
  }
}

export default Root;
