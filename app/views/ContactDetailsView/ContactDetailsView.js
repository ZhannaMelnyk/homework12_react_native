import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import ContactDetailsAvatar from '../../components/ContactDetailsAvatar';
import getAvatarInitials from '../../helpers/getAvatarInitials';
import call from 'react-native-phone-call'

class ContactDetailsView extends Component {
  callHandler = () => {
    const { contact } = this.props.route.params;
    const args = {
      number: contact.phoneNumbers[0].number,
      prompt: false
    }
    call(args).catch(console.error);
  };

  render() {
    const { contact } = this.props.route.params;
    return (
      <View style={styles.container}>
        <ContactDetailsAvatar
          img={
            contact.hasThumbnail
              ? { uri: contact.thumbnailPath }
              : undefined
          }
          placeholder={getAvatarInitials(
            `${contact.givenName} ${contact.familyName}`
          )}
        />
        <View>
          <Text style={styles.contactName}>{contact.givenName} {contact.familyName}</Text>
          {
            contact.phoneNumbers.map(phoneNumber => {
              return <Text
                key={phoneNumber.number}
                style={styles.contactNumbers}
              >
                {phoneNumber.number}
              </Text>
            })
          }
        </View>
        {
          contact.phoneNumbers[0] !== undefined
            ? <View style={styles.callBtn}>
              <Button title={'Call'}
                onPress={this.callHandler} />
            </View>
            : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  contactName: {
    marginTop: 10,
    fontSize: 30
  },
  contactNumbers: {
    marginTop: 10,
    fontSize: 15
  },
  callBtn: {
    marginTop: 30
  }
})

export default ContactDetailsView;
