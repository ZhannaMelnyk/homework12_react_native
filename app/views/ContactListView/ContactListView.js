import React, { Component } from 'react';
import {
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
  TextInput,
  ActivityIndicator
} from 'react-native';
import Contacts from 'react-native-contacts';

import ContactListItem from '../../components/ContactListItem';
import SearchBox from '../../components/SearchBox';

class ContactListView extends Component {
  constructor(props) {
    super(props);

    this.search = this.search.bind(this);

    this.state = {
      contacts: [],
      searchPlaceholder: 'Search',
      typeText: null,
      loading: true
    };
  }

  async componentDidMount() {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: 'Contacts',
        message: 'This app would like to view your contacts.'
      }).then(() => {
        this.loadContacts();
      });
    } else {
      this.loadContacts();
    }
  }

  loadContacts() {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        console.warn('Permission to access contacts was denied');
      } else {
        this.setState({ contacts, loading: false });
      }
    });

    Contacts.getCount(count => {
      this.setState({ searchPlaceholder: `Search ${count} contacts` });
    });
  }

  search(text) {
    const phoneNumberRegex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    const emailAddressRegex = /^(([^<>()[\].,;:\s@']+(\.[^<>()[\].,;:\s@']+)*)|('.+'))@(([^<>()[\].,;:\s@']+\.)+[^<>()[\].,;:\s@']{2,})$/i;
    if (text === '' || text === null) {
      this.loadContacts();
    } else if (phoneNumberRegex.test(text)) {
      Contacts.getContactsByPhoneNumber(text, (err, contacts) => {
        this.setState({ contacts });
      });
    } else if (emailAddressRegex.test(text)) {
      Contacts.getContactsByEmailAddress(text, (err, contacts) => {
        this.setState({ contacts });
      });
    } else {
      Contacts.getContactsMatchingString(text, (err, contacts) => {
        this.setState({ contacts });
      });
    }
  }

  onPressContact(contact) {
    this.props.navigation.navigate('ContactDetails', {
      contact: contact
    });
  }

  onDeleteContact(contact) {
    Contacts.deleteContact(contact, () => {
      this.loadContacts();
    })
  }

  onUpdateContact(contact) {
    var text = this.state.typeText;
    this.setState({ typeText: null });
    if (text === null || text === '')
      Contacts.openExistingContact(contact, () => {
        this.loadContacts();
      })
    else {
      var newPerson = {
        recordID: contact.recordID,
        phoneNumbers: [{ label: 'mobile', number: text }]
      }
      Contacts.editExistingContact(newPerson, (err, contact) => {
        if (err) throw err;
      });
    }
  }

  onCreate(contact) {
    Contacts.openContactForm(contact, () => {
      this.loadContacts();
    })
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            paddingLeft: 100,
            paddingRight: 100,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
        </View>
        <SearchBox
          searchPlaceholder={this.state.searchPlaceholder}
          onChangeText={this.search}
        />
        {
          this.state.loading === true ?
            (
              <View style={styles.spinner}>
                <ActivityIndicator size='large' color='#0000ff' />
              </View>
            ) : (
              <ScrollView style={{ flex: 1 }}>
                {this.state.contacts.map(contact => {
                  return (
                    <ContactListItem
                      contact={contact}
                      key={contact.recordID}
                      title={`${contact.givenName} ${contact.familyName}`}
                      description={`${contact.company}`}
                      onPress={() => this.onPressContact(contact)}
                      onDelete={() => {
                        this.onDeleteContact(contact);
                      }}
                      onUpdate={() => {
                        this.onUpdateContact(contact);
                      }}
                    />
                  );
                })}
              </ScrollView>
            )
        }

        <View style={{ paddingLeft: 10, paddingRight: 10 }}>
          <TextInput
            keyboardType='number-pad'
            style={styles.inputStyle}
            placeholder='Enter number to add to contact'
            onChangeText={text => this.setState({ typeText: text })}
            value={this.state.typeText}
            onSubmitEditing={() => {
              const newPerson = {
                familyName: '',
                givenName: '',
                phoneNumbers: [{
                  label: 'mobile',
                  number: this.state.typeText,
                }],
              }
              this.onCreate(newPerson);
              this.setState({ typeText: '' })
            }}
          />
        </View>

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  spinner: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center'
  },
  inputStyle: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    textAlign: 'center'
  }
});

export default ContactListView;
