import React, { Component } from 'react';
import {
  View,
  TouchableHighlight,
  Text,
  StyleSheet,
  Animated
} from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Avatar from '../Avatar';

import getAvatarInitials from '../../helpers/getAvatarInitials';

class ContactListItem extends Component {
  renderRightAction = (color, x, progress, btn) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0]
    });

    const pressHandler = () => {
      if (btn === 'Delete') {
        this.props.onDelete();
      } else if (btn === 'Update') {
        this.props.onUpdate();
      }
      this.close();
    };

    return (
      <Animated.View style={{ flex: 1, transform: [{ translateX: trans }] }}>
        <RectButton
          style={[styles.rightAction, { backgroundColor: color }]}
          onPress={pressHandler} >
          <Text style={{ color: '#fff' }}>{btn}</Text>
        </RectButton>
      </Animated.View>
    );
  };

  renderRightActions = progress => (
    <View style={{ width: 120, flexDirection: 'row' }}>
      {this.renderRightAction('#00b000', 120, progress, 'Update')}
      {this.renderRightAction('#ef5350', 60, progress, 'Delete')}
    </View>
  );

  updateRef = ref => {
    this.swipeableRow = ref;
  };

  close = () => {
    this.swipeableRow.close();
  };

  render() {
    const {
      title,
      onPress,
      onLongPress,
      disabled
    } = this.props;

    const Component = onPress || onLongPress ? TouchableHighlight : View;

    const {
      itemContainer,
      contactNameSection,
      contactName,
      titleStyle,
    } = styles;

    return (
      <Swipeable
        ref={this.updateRef}
        friction={1}
        renderRightActions={this.renderRightActions}
      >
        <Component
          onPress={onPress}
          onLongPress={onLongPress}
          disabled={disabled}
          underlayColor='#f2f3f5'
        >
          <View style={itemContainer}>
            <Avatar
              img={
                this.props.contact.hasThumbnail
                  ? { uri: this.props.contact.thumbnailPath }
                  : undefined
              }
              placeholder={getAvatarInitials(
                `${this.props.contact.givenName} ${this.props.contact.familyName}`
              )}
              width={40}
              height={40}
            />
            <View style={contactNameSection}>
              <View style={contactName}>
                <Text style={titleStyle}>{title}</Text>
              </View>
            </View>
          </View>
        </Component>
      </Swipeable>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    minHeight: 44,
    height: 70,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: '#515151'
  },
  contactNameSection: {
    marginLeft: 15,
    flexDirection: 'row',
    flex: 20
  },
  contactName: {
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 1
  },
  titleStyle: {
    fontSize: 18
  },
  rightAction: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  }
});

export default ContactListItem;